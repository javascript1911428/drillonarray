function reduce(elements, cb, startingValue) {
    if(Array.isArray(elements)) {

        let index;
        if(startingValue !== undefined) {
            let acc = startingValue;
            index = 0;
            for(index; index < elements.length; index++) {
                acc = cb(acc, elements[index], index, elements);
            }
            return acc
        } else {
            let acc = elements[0];
            index = 1;
            for(index; index < elements.length; index++) {
                acc = cb(acc, elements[index], index, elements);
            }
            return acc
        }

        


    } else {
        return [];
        
    }

    
}

module.exports = reduce;