let each_array = [];

function each(elements, cb) {
    if(Array.isArray(elements)) {
        for(index= 0; index < elements.length; index++) {
            each_array.push(cb(elements,index))
        }
        return each_array;
    } else {
        return [];
    }
    
}

module.exports = each