function find(elements, cb) {
    if(Array.isArray(elements)) {
        let value;
        for(let index = 0; index < elements.length; index++) {
            value = cb(elements[index])
            if(value == true) {
                return elements[index]
            }
        }
        return "undefined"
    } else {
        return [];
    }
    
}

module.exports = find;