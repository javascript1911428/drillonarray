
function filter(elements,cb) {
    if(Array.isArray(elements)) {
        let filter_array = [];
        for(let index = 0; index < elements.length; index++) {
            let key = elements[index];
            let value = cb(key, index, elements);
            if(value === true) {
                filter_array.push(elements[index]);
            }
        
    }
    return filter_array
    } else {
        return [];
    }
    
}

module.exports = filter;