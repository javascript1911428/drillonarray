
function map(elements, cb) {
    if (!Array.isArray(elements) || typeof (cb) != "function" || elements.lenght === 0) {
        return [];
    } 

    const map_array = [];
        for (let index = 0; index < elements.length; index++) {
            const value = (cb(elements[index], index, elements))
            
            map_array.push(value)
        }
    return map_array;
}

module.exports = map;