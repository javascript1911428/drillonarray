
function flatten(elements, n = 1) {
    let flattend_array = [];
    let depth = 0;
    for (let index = 0; index < elements.length; index++) {
        if (Array.isArray(elements[index]) && depth < n) {
            flattend_array = flattend_array.concat(flatten(elements[index], n - 1))
        } else if (elements[index] !== undefined) {
            flattend_array.push(elements[index]);
        }
    }
    return flattend_array;
}


module.exports = flatten;